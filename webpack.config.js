/* eslint-disable no-undef */ // `require` doesn't exist.
/* eslint-disable @typescript-eslint/no-var-requires */ // `module`, `__dirname` doesn't exist.

const CompressionPlugin = require("compression-webpack-plugin");
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const ImageMinimizerPlugin = require("image-minimizer-webpack-plugin");
const zlib = require("zlib");
const zopfli = require("@gfx/zopfli");
const Path = require("path");
const { GenerateSW } = require('workbox-webpack-plugin');

module.exports = (env, argv) =>
{
    argv.mode ??= "development";
    const IsDevelopment = argv.mode === "development";
    const IsProduction = !IsDevelopment;

    const BuiltFileName = IsProduction ? "[name].[contenthash]" : "[name]";

    let additionalPlugins = [
        new MiniCssExtractPlugin({
            filename: `${BuiltFileName}.min.css`,
        }),
        new ImageMinimizerPlugin({
            test: /\.(jpe?g|png|gif|svg)$/i,
            minimizer: {
                implementation: ImageMinimizerPlugin.imageminMinify,
                options: {
                    plugins: [
                        ["gifsicle", { interlaced: true }],
                        ["jpegtran", { progressive: true }],
                        ["optipng", { optimizationLevel: 5 }],
                        // Svgo configuration here https://github.com/svg/svgo#configuration
                        [
                            "svgo",
                            {
                                plugins: [
                                    {
                                        name: "preset-default",
                                        params: {
                                            overrides: {
                                                removeViewBox: false,
                                                addAttributesToSVGElement: {
                                                    params: {
                                                        attributes: [
                                                            { xmlns: "http://www.w3.org/2000/svg" },
                                                        ],
                                                    },
                                                },
                                            },
                                        },
                                    },
                                ],
                            },
                        ],
                    ],
                },
            },
        }),
    ];

    if (IsProduction)
    {
        additionalPlugins.push(
            new CompressionPlugin({
                filename: "[path][base].gz",
                test: /\.(js|css|html?|svg|map|ico|json|wasm|xml)$/,
                compressionOptions: {
                    numiterations: 15,
                },
                algorithm(input, compressionOptions, callback)
                {
                    return zopfli.gzip(input, compressionOptions, callback);
                },
                minRatio: Number.MAX_SAFE_INTEGER,
                threshold: 0,
            }),
            new CompressionPlugin({
                filename: "[path][base].br",
                algorithm: "brotliCompress",
                test: /\.(js|css|html?|svg|map|ico|json|wasm|xml)$/,
                compressionOptions: {
                    params: {
                        [zlib.constants.BROTLI_PARAM_QUALITY]: 11,
                    },
                },
                deleteOriginalAssets: false,
                minRatio: Number.MAX_SAFE_INTEGER,
                threshold: 0,
            }),
        );
    }

    let baseConfig = {
        mode: argv.mode,
        performance: {
            hints: "warning", // can be false if you want to disable these.
            maxEntrypointSize: 64 * 1024 * 1024,
            maxAssetSize: 64 * 1024 * 1024,
        },

        output: {
            filename: `${BuiltFileName}.min.js`,
            path: Path.join(__dirname, "build"),
            publicPath: "/",
        },

        devtool: IsDevelopment ? "source-map" : undefined,

        resolve: {
            extensions: [".ts", ".tsx", ".js"],
            plugins: [new TsconfigPathsPlugin({ configFile: "./tsconfig.json" })],
        },

        module: {
            rules: [
                { test: /\.worker\.ts$/, use: "ts-loader" },
                { test: /\.tsx?$/, use: "ts-loader", exclude: [/\.worker\.ts$/] },
                {
                    test: /\.scss$/,
                    use: [
                        MiniCssExtractPlugin.loader,
                        { loader: "css-loader", options: { importLoaders: 1 } },
                        { loader: "postcss-loader" },
                        {
                            loader: "sass-loader",
                            options: {
                                implementation: require("sass"),
                                webpackImporter: false,
                                sourceMap: IsDevelopment,
                                sassOptions: {
                                    includePaths: ["node_modules"]
                                },
                            }
                        }
                    ]
                },
                {
                    test: /\.json5$/i,
                    loader: "json5-loader",
                    type: "javascript/auto",
                },
            ]
        },
    };

    let result = [];

    result.push({
        ...baseConfig,
        entry: {
            "index": Path.join(__dirname, "ts", "index.tsx"),
            "index-style": Path.join(__dirname, "sass", "index.scss"),
        },
        plugins: [
            new HtmlWebpackPlugin({
                filename: "index.html",
                template: Path.join(__dirname, "html", "index.html"),
                chunks: ["index", "index-style"],
            }),
            new CopyWebpackPlugin({
                patterns: [
                    { from: `./assets/img`, to: "img" },
                    { from: `./assets/manifests`, to: "manifests" },
                ]
            }),
        ].concat(additionalPlugins)
    });

    const keepscoreSPAPaths = [
        "keepscore/index.html",
        "keepscore/newGame.html",
        "keepscore/newGamePlayerNames.html",
        "keepscore/game.html",
    ];
    result.push({
        ...baseConfig,
        entry: {
            "keepscore": Path.join(__dirname, "ts", "keepscore", "index.tsx"),
            "keepscore-style": Path.join(__dirname, "sass", "keepscore.scss"),
        },
        plugins: [
            ...keepscoreSPAPaths.map(p => new HtmlWebpackPlugin({
                filename: p,
                template: Path.join(__dirname, "html", "keepscore.html"),
                chunks: ["keepscore", "keepscore-style"],
            })),
            new GenerateSW({
                additionalManifestEntries: [
                ],
                swDest: "keepscore/service-worker.js",
            }),
        ].concat(additionalPlugins)
    });

    result.push({
        ...baseConfig,
        output: {
            path: Path.join(__dirname, "build"),
        },
        entry: {},
        plugins: [
            new CleanWebpackPlugin(),
        ],
    });

    return result;
};