/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        "./ts/**/*.{js,jsx,ts,tsx}",
    ],
    theme: {
        extend: {},
    },
    plugins: [
        require("daisyui"),
    ],
};
