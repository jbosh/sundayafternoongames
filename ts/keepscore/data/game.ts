export interface Player
{
    color: string;
    name: string;
    score: number;
}

export const CurrentGameVersion = 0;
export interface Game
{
    /**
     * Serialization version number for this game.
     */
    version: number;

    /**
     * ID of the game.
     */
    id: string;

    /**
     * Name of the game (optional).
     */
    name: string | null;

    /**
     * Date the game was created.
     */
    date: Date;

    /**
     * Information about players in the game.
     */
    players: Player[];

    /**
     * Scores (by player index) for each player.
     */
    scores: number[][];
}

const GamePrefix = "game:";

export function GenerateGameID(): string
{
    if (process.env.NODE_ENV === "development")
    {
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let result = ' ';
        const charactersLength = characters.length;
        for (let i = 0; i < 16; i++)
        {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }

        return result;
    }
    else
    {
        return crypto.randomUUID();
    }
}
function GetLocalStorageKey(game: Game): string;
function GetLocalStorageKey(gameID: string): string;
function GetLocalStorageKey(game: Game | string): string
{
    if (typeof game === "string")
        return `${GamePrefix}${game}`;
    else
        return `${GamePrefix}${game.id}`;
}

function ParseJson(json: string): Game
{
    let game = JSON.parse(json);
    game.date = new Date(game.date);
    return game;
}

export function SaveGame(game: Game): void
{
    let key = GetLocalStorageKey(game);
    localStorage.setItem(key, JSON.stringify(game));
}

export function LoadGame(gameID: string): Game | null
{
    let key = GetLocalStorageKey(gameID);
    let json = localStorage.getItem(key);
    if (json === null)
        return null;

    let game = ParseJson(json);
    return game;
}

export function ListGames(): Game[]
{
    let results = [];
    for (let key in localStorage)
    {
        if (!key.startsWith(GamePrefix))
            continue;
        let json = localStorage[key];
        let game = ParseJson(json);
        results.push(game);
    }

    return results;
}