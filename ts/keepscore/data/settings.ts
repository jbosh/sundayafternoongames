const settingsKey = "settings";
interface Settings
{
    defaultNumPlayers: number;
}

export function getSettings(): Settings
{
    let result = localStorage.getItem(settingsKey) as Settings | null;
    if (result == null)
    {
        result = {
            defaultNumPlayers: 2,
        };
    }

    return result;
}