import React, { useState } from "react";
import { Trans, useTranslation } from "react-i18next";
import { useNavigate } from "react-router";
import { BackButton } from "./backButton";
import { CurrentGameVersion, Game, GenerateGameID, Player, SaveGame } from "./data/game";
import { DisableSleep } from "./utilities";

const DefaultColors = [
    "#db1119",
    "#0d21ec",
    "#f28b26",
    "#9b1b97",
    "#1fbb1f",
    "#41b4e1",
    "#cc83f5",
    "#ff008c",
    "#000000",
    "#FFFFFF",
    "#AAAAAA",
    "#A4C639",
    "#e2e63d",
    "#6a3510",
    "#489e99",
    "#f19e99",
];

function GetDefaultPlayers(): Player[]
{
    let params = (new URL(window.location.href)).searchParams;
    let count = +(params.get("count") ?? "0");
    if (count === 0)
    {
        const navigate = useNavigate();
        navigate("/keepscore/newGame", { replace: true });
        return [];
    }

    return Array(count).fill(0).map((_, i) =>
    {
        return {
            color: DefaultColors[i],
            name: "",
            score: 0,
        };
    });
}

export function NewGamePlayerNames()
{
    const navigate = useNavigate();
    const { t } = useTranslation();
    let [players, setPlayers] = useState(GetDefaultPlayers());

    const CreateGame = () =>
    {
        let gameID = GenerateGameID();
        for (let i = 0; i < players.length; i++)
        {
            let player = players[i];
            if (player.name === "")
            {
                player.name = `${t("text_player")} ${i + 1}`;
            }
        }

        let game: Game = {
            version: CurrentGameVersion,
            id: gameID,
            name: null,
            date: new Date(),
            players,
            scores: Array(players.length).fill([]),
        };
        SaveGame(game);
        DisableSleep();
        navigate(`/keepscore/game?id=${gameID}`);
    };

    return <div className="root-new-game">
        <BackButton to="/keepscore/newGame" />
        <div className="content">
            <form onSubmit={e => { e.preventDefault(); CreateGame(); }}>
                <h1><Trans>text_player_names</Trans></h1>
                {players.map((p, i) => <div className="player-row" key={i}>
                    <div className="color" style={{ backgroundColor: p.color }} />
                    <input className="name" type="text" name={`player${i}`} value={p.name} placeholder={`${t("text_player")} ${i + 1}`} onChange={e =>
                    {
                        p.name = e.target.value;
                        setPlayers([...players]);
                    }} />
                </div>)}
                <button className="btn" type="submit" onClick={() => CreateGame()}>
                    <Trans>OK</Trans>
                </button>
            </form>
        </div>
    </div>;
}
