import { NoSleep } from "./noSleep";

export function OpenFullscreen(element: HTMLElement): void;
export function OpenFullscreen(): void;
export function OpenFullscreen(element?: HTMLElement)
{
    element ??= document.body;
    if (element.requestFullscreen)
    {
        element.requestFullscreen();
    }
    else if ((element as any).webkitRequestFullscreen)
    {
        // Safari
        (element as any).webkitRequestFullscreen();
    }
    else if ((element as any).msRequestFullscreen)
    {
        // IE11
        (element as any).msRequestFullscreen();
    }
}

export function CloseFullscreen(): void
{
    if (document.exitFullscreen)
    {
        document.exitFullscreen();
    }
    else if ((document as any).webkitExitFullscreen)
    {
        // Safari
        (document as any).webkitExitFullscreen();
    }
    else if ((document as any).msExitFullscreen)
    {
        // IE11
        (document as any).msExitFullscreen();
    }
}

export function IsFullscreen(): boolean
{
    return (document as any).webkitIsFullScreen
        || (document as any).mozFullScreen
        || (document as any).msFullscreenElement;
}

export function addFullscreenListener(handler: (ev: Event) => void): void
{
    document.addEventListener("fullscreenchange", handler);
    (document as any).addEventListener("mozfullscreenchange", handler);
    (document as any).addEventListener("MSFullscreenChange", handler);
    (document as any).addEventListener("webkitfullscreenchange", handler);
}


export function removeFullscreenListener(handler: (ev: Event) => void): void
{
    document.removeEventListener("fullscreenchange", handler);
    (document as any).removeEventListener("mozfullscreenchange", handler);
    (document as any).removeEventListener("MSFullscreenChange", handler);
    (document as any).removeEventListener("webkitfullscreenchange", handler);
}

const noSleep = new NoSleep();
export function DisableSleep(): void
{
    noSleep.enable();
}

export function EnableSleep(): void
{
    noSleep.disable();
}

export function IsStandalone(): boolean
{
    let isInWebAppiOS = (window.navigator as any).standalone === true;
    let isInWebAppChrome = window.matchMedia("(display-mode: standalone)").matches;
    return isInWebAppChrome || isInWebAppiOS;
}