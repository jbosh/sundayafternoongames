import React from "react";
import { BackButton } from "./backButton";

import { Game, LoadGame, SaveGame } from "./data/game";
import { addFullscreenListener, CloseFullscreen, DisableSleep, EnableSleep, IsFullscreen, IsStandalone, OpenFullscreen, removeFullscreenListener } from "./utilities";

interface RetireScore
{
    playerIndex: number;
    value: number;
    timeout: NodeJS.Timeout | number;
}

interface GamePageState
{
    game: Game;
    isFullscreen: boolean;
}

export class GamePage extends React.Component<any, GamePageState>
{
    private retireScore: RetireScore | null = null;
    private onBeforeUnloadCallback = () => this.onBeforeUnload();
    private onFullscreenChangeCallback = () => this.onFullscreenChange();

    constructor(props: any)
    {
        super(props);

        let params = (new URL(window.location.href)).searchParams;
        let gameID = params.get("id");
        if (gameID === null)
        {
            window.location.href = "/keepscore";
            return;
        }

        let game = LoadGame(gameID);
        if (game === null)
        {
            window.location.href = "/keepscore";
            return;
        }

        this.state = {
            game,
            isFullscreen: IsFullscreen(),
        };
    }

    public override componentDidMount(): void
    {
        addFullscreenListener(this.onFullscreenChangeCallback);

        document.body.classList.add("oled");
        window.addEventListener("beforeunload", this.onBeforeUnloadCallback);
    }

    public override componentWillUnmount(): void
    {
        removeFullscreenListener(this.onFullscreenChangeCallback);

        EnableSleep();
        if (!IsStandalone())
        {
            if (IsFullscreen())
            {
                CloseFullscreen();
            }
        }

        document.body.classList.remove("oled");
        this.tryToRetireScore();
        window.removeEventListener("beforeunload", this.onBeforeUnloadCallback);
    }

    private onBeforeUnload(): void
    {
        this.tryToRetireScore();
    }

    private onFullscreenChange(): void
    {
        this.setState({ isFullscreen: IsFullscreen() });
    }

    private toggleFullscreen(): void
    {
        if (this.state.isFullscreen)
            CloseFullscreen();
        else
            OpenFullscreen();
    }

    private modifyScore(playerIndex: number, value: number): void
    {
        DisableSleep();

        let retireScore: RetireScore | null;
        retireScore = this.retireScore;
        if (retireScore !== null)
        {
            clearTimeout(retireScore.timeout);
            if (retireScore.playerIndex !== playerIndex)
            {
                this.state.game.scores[retireScore.playerIndex].push(retireScore.value);
                retireScore = null;
            }
        }

        if (retireScore === null)
        {
            retireScore = {
                playerIndex,
                value: 0,
                timeout: -1,
            };
        }

        retireScore.value += value;
        let game = this.state.game;
        game.players[playerIndex].score += value;
        retireScore.timeout = setTimeout(() => this.tryToRetireScore(), 10 * 1000);

        this.retireScore = retireScore;
        this.setState({ game: { ...game } });
    }

    private tryToRetireScore(): void
    {
        let retireScore = this.retireScore;
        if (retireScore !== null)
        {
            let game = this.state.game;
            game.scores[retireScore.playerIndex].push(retireScore.value);
            SaveGame(game);
            this.retireScore = null;
            this.setState({ game: { ...game } });
        }
    }

    public override render(): JSX.Element
    {
        return <div className="root-game">
            <BackButton to="/keepscore" />
            {!IsStandalone() && <FullscreenButton className="fullscreen-btn" isFullscreen={this.state.isFullscreen} onClick={() => this.toggleFullscreen()} />}
            {this.state.game.players.map((_, i) => this.renderPlayer(i))}
        </div>;
    }

    private renderPlayer(playerIndex: number): JSX.Element
    {
        let player = this.state.game.players[playerIndex];
        let scores = this.state.game.scores[playerIndex];

        let retireScore = this.retireScore;
        return <div className="player" key={playerIndex}>
            <div className="padding">
                <div className="controls">
                    <div className="name">
                        {player.name}
                        {retireScore?.playerIndex === playerIndex && <div className="score">
                            {retireScore.value >= 0 ? "+" : "-"}{Math.abs(retireScore.value)}
                        </div>}
                    </div>
                    <div className="score">
                        <button className="btn" onClick={() => this.modifyScore(playerIndex, -1)}>-</button>
                        <div className="value">{player.score}</div>
                        <button className="btn" onClick={() => this.modifyScore(playerIndex, 1)}>+</button>
                    </div>
                    <div className="buttons">
                        <button className="btn" onClick={() => this.modifyScore(playerIndex, -10)}>-10</button>
                        <button className="btn" onClick={() => this.modifyScore(playerIndex, -5)}>-5</button>
                        <button className="btn" onClick={() => this.modifyScore(playerIndex, 5)}>+5</button>
                        <button className="btn" onClick={() => this.modifyScore(playerIndex, 10)}>+10</button>
                    </div>
                </div>
                <div className="history">
                    {scores
                        .map((_, i) => scores[scores.length - i - 1])
                        .map((v, i) => <div key={i} className={v < 0 ? "neg" : "pos"}>{v}</div>)
                    }
                </div>
            </div>
        </div>;
    }
}

function FullscreenButton(props: { isFullscreen: boolean, className?: string, onClick: () => void }): JSX.Element
{
    return <div className={props.className} onClick={props.onClick}>
        {props.isFullscreen
            ? <svg version="1.1" x="0px" y="0px" width="48px" height="48px" viewBox="0 0 48 48">
                <polyline points="11.3,19.2 11.3,11.2 19.8,11.2 " />
                <polyline points="28.7,11.2 36.7,11.2 36.7,19.6 " />
                <polyline points="36.7,28.8 36.7,36.8 28.2,36.8 " />
                <polyline points="19.3,36.8 11.3,36.8 11.3,28.4 " />
            </svg>
            : <svg version="1.1" x="0px" y="0px" width="48px" height="48px" viewBox="0 0 48 48">
                <polyline points="28.9,37.1 28.9,29.1 37.3,29.1 " />
                <polyline points="11.1,29.1 19.1,29.1 19.1,37.5 " />
                <polyline points="19.1,10.9 19.1,18.9 10.7,18.9 " />
                <polyline points="36.9,18.9 28.9,18.9 28.9,10.5 " />
            </svg>
        }
    </div>;
}