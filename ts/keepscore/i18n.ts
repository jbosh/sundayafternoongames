import i18n from "i18next";
import LanguageDetector from 'i18next-browser-languagedetector';
import { initReactI18next } from "react-i18next";

import Languages from "./translations/languages.json";

i18n
    .use(LanguageDetector)
    .use(initReactI18next)
    .init({
        resources: Languages,
        fallbackLng: "en",
        supportedLngs: ["fr", "en"],

        interpolation: {
            escapeValue: false,
        },
        debug: process.env.NODE_ENV === "development",
    });

export default i18n;