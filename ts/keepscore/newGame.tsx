import React, { useState } from "react";
import { Trans } from "react-i18next";
import { Link } from "react-router-dom";
import { BackButton } from "./backButton";
import { getSettings } from "./data/settings";


export function NewGame()
{
    let [playerCount, setPlayerCount] = useState(getSettings().defaultNumPlayers);

    return <div className="root-new-game">
        <BackButton to="/keepscore" />
        <div className="content">
            <h1><Trans>button_new_game</Trans></h1>
            <label>
                <Trans>text_num_players</Trans>:
                <input type="number" value={playerCount} onChange={e => setPlayerCount(e.target.valueAsNumber)} min={2} max={32} />
            </label>
            <div>
                <Link className="btn" to={`/keepscore/newGamePlayerNames?count=${playerCount}`}>
                    <Trans>menu_add_edit_players</Trans>
                </Link>
            </div>
        </div>
    </div>;
}
