import React from "react";
import { createRoot } from "react-dom/client";
import { Route, Routes } from "react-router";
import { BrowserRouter } from "react-router-dom";
import { NewGame } from "./newGame";
import { SavedGames } from "./savedGames";
import i18next from "./i18n";
import { I18nextProvider } from "react-i18next";
import { NewGamePlayerNames } from "./newGamePlayerNames";
import { GamePage } from "./gamePage";

function KeepScore()
{
    return <I18nextProvider i18n={i18next}>
        <BrowserRouter>
            <Routes>
                <Route path="/keepscore/" element={<SavedGames />} />
                <Route path="/keepscore/newGame" element={<NewGame />} />
                <Route path="/keepscore/newGamePlayerNames" element={<NewGamePlayerNames />} />
                <Route path="/keepscore/game" element={<GamePage />} />
            </Routes>
        </BrowserRouter>
    </I18nextProvider>;
}

window.addEventListener("load", async () =>
{
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    let root = createRoot(document.getElementById("content-root")!);
    root.render(<KeepScore />);
    if ("serviceWorker" in navigator)
    {
        if (process.env.NODE_ENV === "development")
        {
            let registrations = await navigator.serviceWorker.getRegistrations();
            for (let registration of registrations)
            {
                registration.unregister();
            }
        }

        try
        {
            await navigator.serviceWorker.register("/keepscore/service-worker.js", {
                scope: "/keepscore/",
            });
        }
        catch (err)
        {
            console.error(err);
        }
    }
});