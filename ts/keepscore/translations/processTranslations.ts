import * as fs from "fs";
import * as Path from "path";
import * as cheerio from "cheerio";

let paths = fs.readdirSync(__dirname).map(f => Path.join(__dirname, f));

let result = {} as any;

for (let path of paths)
{
    let parsedPath = Path.parse(path);
    let languageCode = parsedPath.name;

    let $ = cheerio.load(fs.readFileSync(path),
        {
            xml: {
                xmlMode: true,
            }
        });

    let lang = {} as any;
    $("string").each((_, element) =>
    {
        let name = element.attribs["name"];
        let child = element.firstChild as Text | null;
        if (child)
        {
            let value = child.data;
            lang[name] = value;
        }
    });

    $("plurals").each((_, element) =>
    {
        let name = element.attribs["name"];
        $("item", element).each((_, itemElement) =>
        {
            let quantity = itemElement.attribs["quantity"];
            let child = itemElement.firstChild as Text | null;
            if (child)
            {
                let value = child.data;

                value = value.replace("%d", "{{count}}");
                lang[`${name}_${quantity}`] = value;
            }
        });
    });
    result[languageCode] = {
        translation: lang
    };
}

fs.writeFileSync(Path.join(__dirname, "languages.json"), JSON.stringify(result), "utf8");
