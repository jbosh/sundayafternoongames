import React from "react";
import { Link } from "react-router-dom";

export function BackButton(props: { to: string })
{
    return <Link to={props.to} className="back-btn">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48" width={32} height={32}>
            <polyline points="25,9 9.9,24 25,39" fill="none" />
            <line x1="40" y1="24" x2="10.6" y2="24" fill="none" />
        </svg>
    </Link>;
}