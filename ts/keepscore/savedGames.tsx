import React from "react";
import { Trans } from "react-i18next";
import { Link } from "react-router-dom";
import { ListGames } from "./data/game";
import { SettingsButton } from "./settingsButton";
import { EnableSleep } from "./utilities";

export function SavedGames()
{
    EnableSleep();
    let games = ListGames();
    games.sort((a, b) => b.date.getTime() - a.date.getTime());

    return <div className="root-saved-games">
        <div className="content">
            <div className="heading">
                <h1>KeepScore</h1>
                <Link className="btn new-game-btn" to="/keepscore/newGame"><Trans>button_new_game</Trans></Link>
                <a className="btn svg-btn">
                    <SettingsButton />
                </a>
            </div>
            <div className="saved-game-list">
                <div className="heading">
                    <em><Trans>text_saved_games</Trans> {games.length !== 0 && `(${games.length})`}</em>
                    
                </div>
                <div className="games">
                    {games.length === 0
                        ? <Trans>text_no_saved_games</Trans>
                        : <>
                            {games.map(g => <Link className="game" key={g.id} to={`/keepscore/game?id=${g.id}`}>
                                <div className="player-count">{g.players.length}</div>
                                <div className="players">{g.players.map(p => p.name).join(", ")}</div>
                                <div className="date">{g.date.toLocaleString()}</div>
                            </Link>)}
                        </>}
                </div>
            </div>
        </div>
    </div>;
}