import React from "react";
import { createRoot } from "react-dom/client";

function Index()
{
    return <div>
        Click here for <a href="/keepscore">KeepScore</a>.
    </div>;
}

window.addEventListener("load", async () =>
{
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    let root = createRoot(document.getElementById("content-root")!);
    root.render(<Index />);
});